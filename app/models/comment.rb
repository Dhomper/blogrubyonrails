class Comment < ActiveRecord::Base
	validates_length_of :body, :in => 20..400, :message => "La longitud del body debe ser de 20 a 400 caracteres"
end
