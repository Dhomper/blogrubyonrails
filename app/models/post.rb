class Post < ActiveRecord::Base
	has_many :comments
	validates_presence_of :title, :body
	validates_length_of :body, :in => 10..400, :message => "La longitud del body debe ser de 10 a 400 caracteres"
end
